# Compiled libprotobuf

This image downloads and compiles protobuf library.

header files are in `/usr/local/include`
library files are in `/usr/local/lib`

## Header files
```
/usr/local/include# tree -UhCv --noreport
.
`-- [4.0K]  google
    `-- [4.0K]  protobuf
        |-- [6.0K]  any.h
        |-- [ 14K]  any.pb.h
        |-- [5.7K]  any.proto
        |-- [ 52K]  api.pb.h
        |-- [7.6K]  api.proto
        |-- [ 30K]  arena.h
        |-- [ 12K]  arena_impl.h
        |-- [ 14K]  arenastring.h
        |-- [4.0K]  compiler
        |   |-- [7.4K]  code_generator.h
        |   |-- [ 19K]  command_line_interface.h
        |   |-- [4.0K]  cpp
        |   |   `-- [3.8K]  cpp_generator.h
        |   |-- [4.0K]  csharp
        |   |   |-- [2.7K]  csharp_generator.h
        |   |   `-- [4.0K]  csharp_names.h
        |   |-- [ 14K]  importer.h
        |   |-- [4.0K]  java
        |   |   |-- [2.9K]  java_generator.h
        |   |   `-- [3.5K]  java_names.h
        |   |-- [4.0K]  js
        |   |   |-- [ 16K]  js_generator.h
        |   |   `-- [1.9K]  well_known_types_embed.h
        |   |-- [4.0K]  objectivec
        |   |   |-- [3.2K]  objectivec_generator.h
        |   |   `-- [ 11K]  objectivec_helpers.h
        |   |-- [ 27K]  parser.h
        |   |-- [4.0K]  php
        |   |   `-- [2.9K]  php_generator.h
        |   |-- [4.2K]  plugin.h
        |   |-- [ 66K]  plugin.pb.h
        |   |-- [8.0K]  plugin.proto
        |   |-- [4.0K]  python
        |   |   `-- [7.5K]  python_generator.h
        |   `-- [4.0K]  ruby
        |       `-- [2.6K]  ruby_generator.h
        |-- [ 89K]  descriptor.h
        |-- [581K]  descriptor.pb.h
        |-- [ 36K]  descriptor.proto
        |-- [ 19K]  descriptor_database.h
        |-- [ 10K]  duration.pb.h
        |-- [4.8K]  duration.proto
        |-- [9.9K]  dynamic_message.h
        |-- [8.0K]  empty.pb.h
        |-- [2.4K]  empty.proto
        |-- [ 76K]  extension_set.h
        |-- [ 12K]  extension_set_inl.h
        |-- [ 12K]  field_mask.pb.h
        |-- [8.0K]  field_mask.proto
        |-- [3.9K]  generated_enum_reflection.h
        |-- [3.2K]  generated_enum_util.h
        |-- [ 12K]  generated_message_reflection.h
        |-- [ 12K]  generated_message_table_driven.h
        |-- [9.1K]  generated_message_util.h
        |-- [3.4K]  has_bits.h
        |-- [6.9K]  implicit_weak_message.h
        |-- [9.1K]  inlined_string_field.h
        |-- [4.0K]  io
        |   |-- [ 68K]  coded_stream.h
        |   |-- [5.3K]  io_win32.h
        |   |-- [ 16K]  printer.h
        |   |-- [2.4K]  strtod.h
        |   |-- [ 16K]  tokenizer.h
        |   |-- [ 10K]  zero_copy_stream.h
        |   |-- [ 13K]  zero_copy_stream_impl.h
        |   `-- [ 16K]  zero_copy_stream_impl_lite.h
        |-- [ 43K]  map.h
        |-- [7.0K]  map_entry.h
        |-- [ 25K]  map_entry_lite.h
        |-- [ 30K]  map_field.h
        |-- [ 14K]  map_field_inl.h
        |-- [7.2K]  map_field_lite.h
        |-- [ 38K]  map_type_handler.h
        |-- [ 58K]  message.h
        |-- [ 23K]  message_lite.h
        |-- [3.1K]  metadata.h
        |-- [8.5K]  metadata_lite.h
        |-- [ 29K]  parse_context.h
        |-- [2.0K]  port.h
        |-- [ 13K]  port_def.inc
        |-- [2.9K]  port_undef.inc
        |-- [ 22K]  reflection.h
        |-- [3.6K]  reflection_ops.h
        |-- [ 93K]  repeated_field.h
        |-- [ 13K]  service.h
        |-- [ 11K]  source_context.pb.h
        |-- [2.3K]  source_context.proto
        |-- [ 44K]  struct.pb.h
        |-- [3.7K]  struct.proto
        |-- [4.0K]  stubs
        |   |-- [ 11K]  bytestream.h
        |   |-- [ 17K]  callback.h
        |   |-- [5.6K]  casts.h
        |   |-- [8.2K]  common.h
        |   |-- [5.9K]  fastmem.h
        |   |-- [4.0K]  hash.h
        |   |-- [8.7K]  logging.h
        |   |-- [4.8K]  macros.h
        |   |-- [ 30K]  map_util.h
        |   |-- [6.0K]  mutex.h
        |   |-- [2.1K]  once.h
        |   |-- [5.0K]  platform_macros.h
        |   |-- [ 13K]  port.h
        |   |-- [3.9K]  status.h
        |   |-- [3.2K]  stl_util.h
        |   |-- [ 17K]  stringpiece.h
        |   |-- [ 38K]  strutil.h
        |   `-- [4.7K]  template_util.h
        |-- [ 28K]  text_format.h
        |-- [ 10K]  timestamp.pb.h
        |-- [6.1K]  timestamp.proto
        |-- [105K]  type.pb.h
        |-- [6.0K]  type.proto
        |-- [ 13K]  unknown_field_set.h
        |-- [4.0K]  util
        |   |-- [5.3K]  delimited_message_util.h
        |   |-- [ 10K]  field_comparator.h
        |   |-- [ 10K]  field_mask_util.h
        |   |-- [8.2K]  json_util.h
        |   |-- [ 44K]  message_differencer.h
        |   |-- [ 12K]  time_util.h
        |   |-- [2.8K]  type_resolver.h
        |   `-- [2.4K]  type_resolver_util.h
        |-- [ 16K]  wire_format.h
        |-- [ 82K]  wire_format_lite.h
        |-- [ 65K]  wrappers.pb.h
        `-- [3.9K]  wrappers.proto
```

## Library files

```
/usr/local/lib# tree -UhCv --noreport
.
|-- [ 14M]  libprotobuf-lite.a
|-- [1007]  libprotobuf-lite.la
|-- [  26]  libprotobuf-lite.so -> libprotobuf-lite.so.21.0.1
|-- [  26]  libprotobuf-lite.so.21 -> libprotobuf-lite.so.21.0.1
|-- [5.3M]  libprotobuf-lite.so.21.0.1
|-- [ 88M]  libprotobuf.a
|-- [ 972]  libprotobuf.la
|-- [  21]  libprotobuf.so -> libprotobuf.so.21.0.1
|-- [  21]  libprotobuf.so.21 -> libprotobuf.so.21.0.1
|-- [ 33M]  libprotobuf.so.21.0.1
|-- [135M]  libprotoc.a
|-- [ 988]  libprotoc.la
|-- [  19]  libprotoc.so -> libprotoc.so.21.0.1
|-- [  19]  libprotoc.so.21 -> libprotoc.so.21.0.1
|-- [ 45M]  libprotoc.so.21.0.1
```
