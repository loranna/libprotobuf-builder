FROM ubuntu:18.04
RUN apt-get update && apt-get upgrade -y
RUN apt-get install git autoconf libtool g++ make -y
WORKDIR /libs
RUN git clone https://github.com/protocolbuffers/protobuf.git
WORKDIR /libs/protobuf
RUN git checkout v3.10.1
RUN git submodule update --init --recursive
RUN ./autogen.sh && ./configure && make -j4 && make install

RUN ldconfig
ENTRYPOINT [ "/bin/bash" ]