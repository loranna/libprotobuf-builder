#!/bin/bash

TAG=$(git tag -l --sort=-v:refname | head -n 1 | cut -c 2-)
IFS='.' read -ra vers <<< "$TAG"
MAJOR="${vers[0]}"
MINOR="${vers[1]}"
COMMITCOUNT="${vers[2]}"

docker build . \
  -t registry.gitlab.com/loranna/libprotobuf-builder:$MAJOR.$MINOR.$COMMITCOUNT \
  -t registry.gitlab.com/loranna/libprotobuf-builder:latest
